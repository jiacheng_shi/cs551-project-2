/* If there were any type definitions local to the Process Manager, they would
 * be here.  This file is included only for symmetry with the kernel and File
 * System, which do have some local type definitions.
 */

struct mailbox mailbox_t {

	message buffer[length];
	boolean permission;
	int length;

};

struct message message_t {

	int length;
	message *content;

};
